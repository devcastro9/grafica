'use strict';
const ctx = document.getElementById('myChart').getContext('2d');
// Data
const url = "http://localhost:5068/api/inventarios";

function color() {
    const max = 255;
    const min = 0;
    return Math.random()*(max-min) + min;
}

fetch(url)
    .then(r => r.json())
        .then(data => {
            const nombres = data.map(e => e.nombre);
            const cantidad = data.map(e => e.cantidad);
            const colorg = data.map(e => `rgba(${color()}, ${color()}, ${color()}, 0.2)`);
            const myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: nombres,
                    datasets: [{
                        label: 'Algo',
                        data: cantidad,
                        backgroundColor: colorg,
                        borderColor: colorg,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        })
        .catch(error =>
            {
                document.getElementById('error').innerHTML = 'Ocurrio un error: ' + error.message;
            }
        );
